# redacted

1. Download both images from FTP server with the folders EE_PARA and dbdump
1.a. Images are located under redacted/Dockerimgs  
2. Import images into your docker  
2.a. docker load -i ./redacted-was-1.0.0.tar  
2.b. docker load -i ./redacted-oracle-1.0.0.tar  
3. Create a Docker Image Registry in Openshift (https://docs.openshift.com/enterprise/3.0/install_config/install/docker_registry.html for reference)  
4. Make sure docker is authenticated with your Openshift environment  
4.a. docker login -u <user> -p <password> <openshift_image_registry>  
5. Assure the images are tagged correctly. They should follow the pattern <openshift_registry_url>/redacted/<was|oracle>:1.0.0  
5.a. To get the openshift registry URL, you can run these commands:  
```
  oc project openshift-image-registry
  oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge
  oc policy add-role-to-user registry-viewer <your_ad_email>
  oc policy add-role-to-user registry-editor <your_ad_email>
  OC_HOST=$(oc get route default-route --template='{{ .spec.host }}')
  oc adm policy add-scc-to-user anyuid -z default -n $OC_PROJECT

```
5.b. Retag the image if needed with `docker tag <current_image> $OC_HOST/redacted/<was|oracle>:1.0.0`  
6. Upload both images to the registry  
```
docker push $OC_HOST/redacted/was:1.0.0
docker push $OC_HOST/redacted/oracle:1.0.0
```
7. Deploy Oracle in Openshift  
7.a. Go to Openshift console  
7.b. Go to Developer Mode and select the project that will run the app  
7.c. Click "+ Add" and then select "Container Image"  
7.d. Switch to "Image stream tag from internal registry" and select the Oracle image  
7.e. Under "General", set the name of the application (it will have both Oracle and Websphere deployed in the future)  
7.f. Leave the rest as default and Click "Create"  
8. Redo steps above, but for Websphere (WAS image)  
9. Once all are completed, now you need to expose WAS. To do that:  
9.a. run `oc get -o yaml svc was oracle > wasapp.yaml` to get all Pod manifest into a file name wasapp.yaml  
9.b. Find `spec.type` in both. They should be "ClusterIP". Change them to "LoadBalancer" (exactly like this)  
9.c. run `oc apply -f wasapp.yaml` and wait a couple minutes  
9.d. run `oc get svc` and try to access the External IP of WAS. The full url should be `https://<external_ip>:9043/ibm/console`  
9.e. Write down Oracle's External IP for future use.  
10. Configuring WAS  
10.a. Log in to WAS Console (user: wsadmin, password: oRLAQODQ)  
10.b. Go to Resources -> JDBC -> Datasources -> EXIMS  
10.c. At the bottom, change the connection string from `jdbc:oracle:thin:@172.30.241.111:1521/XEPDB1` and replace with the IP you got from 9.e  
10.d Save and apply the same change to the other 2 datasources.  
10.e. Save to Master and test the connection  
11. Navigate to Application -> Application Types -> Websphere Enterprise, select EximBill and click start  



## Getting started

https://docs.microsoft.com/en-us/azure/openshift/tutorial-create-cluster