#!/bin/bash

# =============== OPENSHIFT
# https://docs.microsoft.com/en-us/azure/openshift/howto-deploy-java-liberty-app
az provider register -n Microsoft.RedHatOpenShift --wait
az provider register -n Microsoft.Compute --wait
az provider register -n Microsoft.Storage --wait
az provider register -n Microsoft.Authorization --wait

LOCATION=eastus                 # the location of your cluster
RESOURCEGROUP=redacted            # the name of the resource group where you want to create your cluster
CLUSTER=redacted-aro-cluster                 # the name of your cluster

az network vnet create \
   --resource-group $RESOURCEGROUP \
   --name aro-vnet \
   --address-prefixes 10.0.0.0/22

az network vnet subnet create \
  --resource-group $RESOURCEGROUP \
  --vnet-name aro-vnet \
  --name master-subnet \
  --address-prefixes 10.0.0.0/23 \
  --service-endpoints Microsoft.ContainerRegistry

az network vnet subnet create \
  --resource-group $RESOURCEGROUP \
  --vnet-name aro-vnet \
  --name worker-subnet \
  --address-prefixes 10.0.2.0/23 \
  --service-endpoints Microsoft.ContainerRegistry

az network vnet subnet update \
  --name master-subnet \
  --resource-group $RESOURCEGROUP \
  --vnet-name aro-vnet \
  --disable-private-link-service-network-policies true

az aro create \
  --resource-group $RESOURCEGROUP \
  --name $CLUSTER \
  --vnet aro-vnet \
  --master-subnet master-subnet \
  --worker-subnet worker-subnet

# GET KUBEADMIN PASSWORD
az aro list-credentials \
  --name $CLUSTER \
  --resource-group $RESOURCEGROUP

# login the portal below using the password from the previous step
az aro show \
  --name $CLUSTER \
  --resource-group $RESOURCEGROUP \
  --query "consoleProfile.url" -o tsv

resource_group=$RESOURCEGROUP
aro_cluster=$CLUSTER
domain=$(az aro show -g $resource_group -n $aro_cluster --query clusterProfile.domain -o tsv)
location=$(az aro show -g $resource_group -n $aro_cluster --query location -o tsv)
apiServer=$(az aro show -g $resource_group -n $aro_cluster --query apiserverProfile.url -o tsv)
webConsole=$(az aro show -g $resource_group -n $aro_cluster --query consoleProfile.url -o tsv)
oauthCallbackURL=https://oauth-openshift.apps.$domain.$location.aroapp.io/oauth2callback/AAD
client_secret=redacted005qqk!
app_id=$(az ad app create \
  --query appId -o tsv \
  --display-name aro-auth \
  --reply-urls $oauthCallbackURL \
  --password $client_secret)
tenant_id=$(az account show --query tenantId -o tsv)
cat > manifest.json<< EOF
[{
  "name": "upn",
  "source": null,
  "essential": false,
  "additionalProperties": []
},
{
"name": "email",
  "source": null,
  "essential": false,
  "additionalProperties": []
}]
EOF
az ad app update \
  --set optionalClaims.idToken=@manifest.json \
  --id $app_id
az ad app permission add \
 --api 00000002-0000-0000-c000-000000000000 \
 --api-permissions 311a71cc-e848-46a1-bdf8-97ff7156d8e6=Scope \
 --id $app_id

kubeadmin_password=$(az aro list-credentials \
  --name $aro_cluster \
  --resource-group $resource_group \
  --query kubeadminPassword --output tsv)
oc login $apiServer -u kubeadmin -p $kubeadmin_password
oc create secret generic openid-client-secret-azuread \
  --namespace openshift-config \
  --from-literal=clientSecret=$client_secret
cat > oidc.yaml<< EOF
apiVersion: config.openshift.io/v1
kind: OAuth
metadata:
  name: cluster
spec:
  identityProviders:
  - name: AAD
    mappingMethod: claim
    type: OpenID
    openID:
      clientID: $app_id
      clientSecret:
        name: openid-client-secret-azuread
      extraScopes:
      - email
      - profile
      extraAuthorizeParameters:
        include_granted_scopes: "true"
      claims:
        preferredUsername:
        - email
        - upn
        name:
        - name
        email:
        - email
      issuer: https://login.microsoftonline.com/$tenant_id
EOF

apiServer=$(az aro show -g $RESOURCEGROUP -n $CLUSTER --query apiserverProfile.url -o tsv)
oc login $apiServer -u kubeadmin -p RHupz-VumRs-uJat5-5XtZB

oc project openshift-image-registry
oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge
oc policy add-role-to-user registry-viewer dcamizotti@ciandt.com
oc policy add-role-to-user registry-editor dcamizotti@ciandt.com
OC_HOST=$(oc get route default-route --template='{{ .spec.host }}')

OC_PROJECT=redacted
oc new-project $OC_PROJECT
oc adm policy add-role-to-user admin dcamizotti@ciandt.com
oc adm policy add-scc-to-user anyuid -z default -n $OC_PROJECT

# dcamizotti login
oc login --token=sha256~vOq6pKfxuB47_EB1UKrI1wze9-kCP3GHOE50Bc38Qy8 --server=https://api.cszqb9ca.eastus.aroapp.io:6443

oc project $OC_PROJECT
docker login -u $(oc whoami) -p $(oc whoami -t) ${OC_HOST}


# =============== ORACLE
# download redacted dump files from sft and place them (.DMP and .log) inside dbdump folder
# clone https://github.com/oracle/docker-images
# navigate to OracleDatabase/SingleInstance/dockerfiles
sh buildContainerImage.sh -v 18.4.0 -t redacted-oracle-prebuilt -x -i 
docker run --name oracle-build \
      -v $(pwd)/dbdump/:/opt/oracle/admin/XE/dpdump/:Z \
      -p 1521:1521 \
      -p 5500:5500 \
      -d redacted-oracle-prebuilt:latest

docker exec -ti oracle-build /bin/bash
# log in the oracle container and run:
/setPassword.sh oracle
chmod 6751 $ORACLE_HOME/bin/oracle
chmod 777 -R /opt/oracle/admin/XE/dpdump
mkdir /home/datafiles
chmod 777 /home/datafiles
sqlplus SYS/oracle@XEPDB1 as sysdba @dbprep.sh #dbprep.sh is the sh inside dbdump
impdp system/oracle@XEPDB1 dumpfile=MZ_PD_20210915.DMP logfile=MZ_PD_20210915.log 
# IN CASE OF ORA-12547, http://www.dadbm.com/how-to-fix-ora-12547-tns-lost-contact-when-try-to-connect-to-oracle/
# exit the container but keep it running

docker tag redacted-oracle:1.0.0 $OC_HOST/redacted/oracle:1.0.0
docker push $OC_HOST/redacted/oracle:1.0.0

# start the oracle container on Openshift
# go into the portal, log as your user, projects -> redacted, start new app, select container image, image stream and deploy oracle image
# run the below command to get Oracle ClusterIP, it will be used to configure WAS
oc get svc

# =============== WAS Image
docker build . -t redacted-was-builder:latest
docker run --name redacted-was \
      -p 9043:9043 \
      -p 9443:9443 \
      redacted-was-builder:latest
docker exec redacted-was cat /tmp/PASSWORD # user: wsadmin, password oRLAQODQ

# access https://127.0.0.1:9043/ibm/console
# create shared library (Environment -> Shared Library) called EE_LIB pointing to /opt/IBM/WebSphere/AppServer/EE_PARA/EE_LIB
# deploy Eximbills ear from redacted
# go into Eximbills -> Target state -> set to not auto deploy
# add shared lib config to Eximbills EAR -> assign EE_LIB to all options under shared lib references
# server -> server 1 -> java & process management -> process def -> JVM -> custom props -> user.dir, ee.input.dir and ee.output.dir, all pointing to /opt/IBM/WebSphere/AppServer/EE_PARA/
# create datasources: EXIMS EXIMT EXIMSYS
#       jdbc:oracle:thin:@<ORACLE_IP_FROM_ABOVE>:1521/XEPDB1
#       should look like: jdbc:oracle:thin:@172.30.241.111:1521/XEPDB1
# GO INTO EXIMS, JAAS, create EXIMUSER EXIMSYS EXIMT
# go back into each datasource and eddit the connection type to each JAAS created (EXIMS -> EXIMUSER, EXIMSYS -> EXIMSYS, EXIMT -> EXIMTRX)
docker commit redacted-was redacted-was:1.0.0

docker tag redacted-was:1.0.0 $OC_HOST/redacted/was:1.0.0
docker push $OC_HOST/redacted/was:1.0.0

# now we need to add WAS image to the application on Openshift, like we did with Oracle. Once up and running, we need to expose them
# run the command below to get manifest yaml
oc get -o yaml svc was oracle > wasapp.yaml
# change type to LoadBalancer on both
oc apply -f wasapp.yaml
# done! run the below command to get the external IP:
oc get svc

# Log in to WAS console and start eximbills app. Final URL should be something like:
# https://52.224.39.41:9043/ibm/console/secure/securelogon.do
# https://console-openshift-console.apps.cszqb9ca.eastus.aroapp.io/topology/ns/redacted?view=graph