FROM ibmcom/websphere-traditional:9.0.5.8
COPY EE_PARA /opt/IBM/WebSphere/AppServer/EE_PARA/

EXPOSE 8000:9999
EXPOSE 2809

RUN /work/configure.sh